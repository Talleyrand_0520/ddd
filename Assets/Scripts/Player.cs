﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float speed = 10f;
    public float angle = 150f;
    private Vector3 Player_pos;
    float h;

    public GameObject body;
    public tail tail_E;
    public tail tail_T;

    private List<tail> _list = new List<tail>();

    private void Start()
    {
        Player_pos = this.transform.position;
    }

    void Update()
    {
        Inputs();
        UpdateTransform();


    }

    void Inputs()
    {
        h = Input.GetAxis("Horizontal") * angle;
    }

    void UpdateTransform ()
    {
        if (null == this.gameObject)
            return;

        transform.Translate(Vector3.forward * speed * Time.deltaTime);

        transform.Rotate(Vector3.up * h * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("PickedUpItem"))
        {
            Item hitObject = collision.gameObject.GetComponent<Consumable>().item;

            if(hitObject != null)
            {
                print("Eat: " + hitObject.objectName);

                GameObject newBody = Instantiate(body, Player_pos, Quaternion.identity) as GameObject;
                tail t = newBody.GetComponent<tail>();
                t.target = List<tail>.Add(newBody);
                

                
                //switch (hitObject.itemType)
                //{
                //    case Item.ItemType.COIN:
                //        break;
                //    default:
                //        break;
                //}

                //collision.gameObject.SetActive(false);
                Destroy(collision.gameObject); //1. 지워버린다
            }
        }
    }
}
