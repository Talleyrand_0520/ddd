﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tail : MonoBehaviour
{
    public Transform target;
    public Vector3 offset = new Vector3(0f, 0f, 1f);
    public float speed = 10f;

    public tail next;
    void Start()
    {
        transform.position = target.position + offset;
    }

    // Update is called once per frame
    void Update()
    {
        if ((target.position - transform.position).magnitude > 1)
        {
            transform.Translate(Vector3.forward * Time.deltaTime * speed);
            transform.LookAt(target);
        }
    }

    public void Move(Vector3 Pos)
    {
        this.transform.position = Pos;
        if (next != null)
        {
            next.Move(this.transform.position);
        }
    }
}
